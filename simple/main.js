var app = new Vue({
    el: '#app',
    data: {
        product: 'Socks',
        description: 'This is the description of the socks',
        image: "./assets/vmSocks-green-onWhite.jpeg",
        inStock: true,
        details: ["80% cotton", "20% polyster", "Gender-neutral"],

        variants: [
        	{
        		variantId: 2234,
        		variantColor: "green"
        	},
        	{
        		variantId: 2235,
        		variantColor: "blue"
        	}
        ],
        cart: 0,
    },
    methods: {
    	addToCart() {
    		this.cart += 1
    	}
    }
})